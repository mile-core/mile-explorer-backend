#pragma once

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include <iostream>

#include "jsonrpc/context.hpp"


namespace mile {

    inline auto fail(boost::system::error_code ec, char const *what) -> void {
        std::cerr << what << ": " << ec.message() << "\n";
    }

    namespace http {

        namespace http = boost::beast::http;

        template<
                class Body,
                class Allocator,
                class Send,
                class RPC
        >
        auto handle_request(
                http::request<Body, http::basic_fields<Allocator>> &&req,
                Send &&send,
                RPC &&rpc

        ) -> void {
            std::cerr<<"start processing request" <<std::endl;
            std::cerr<<" url : "<<req.target()<<std::endl;
            ///TODO : parser url
/*

            if( req.target().empty()      ||
                req.target()!= "/v1/api"  ||
                req.target()!= "/v1/api/"
            ) {
                return send(std::move(http::response<http::string_body>{http::status::forbidden, req.version()}));
            }
*/
            if (req.method() == http::verb::post) {
                http::response<http::string_body> res;
                res.version(req.version());
                std::cerr << req.body() << std::endl;
                context ctx;
                std::cerr << " body === "<< req.body()<<std::endl;
                ctx.from_string(req.body());
                std::cerr << ctx.request.method << std::endl;

                if (rpc->apply(ctx.request.method, ctx)) {
                    res.body() = ctx.to_json();
                    res.prepare_payload();
                    std::cerr<<"finish processing request: OK" <<std::endl;
                    return send(std::move(res));
                } else {
                    make_response_error_non_method(ctx);
                    res.result(http::status::not_found);
                    res.body() = ctx.to_json();
                    res.prepare_payload();
                    std::cerr<<"finish processing request: not_found" <<std::endl;
                    return send(std::move(res));
                }

            } /// post

            std::cerr<<"finish processing request: method_not_allowed" <<std::endl;
            return send(std::move( http::response<http::empty_body> {http::status::method_not_allowed,req.version()}));


        } /// handle_request



    }
}