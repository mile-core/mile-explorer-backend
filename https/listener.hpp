#pragma once

#include <boost/beast/http.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/stream.hpp>

#include "session.hpp"
#include "rpc/rpc.hpp"



namespace mile {
    namespace https {

        using tcp = boost::asio::ip::tcp;
        namespace ssl = boost::asio::ssl;
        namespace http = boost::beast::http;

        class listener final : public std::enable_shared_from_this<listener> {
            ssl::context &ctx_;
            tcp::acceptor acceptor_;
            tcp::socket socket_;
            shared_rpc rpc;

        public:
            listener(
                    boost::asio::io_context &ioc,
                    ssl::context &ctx,
                    tcp::endpoint endpoint,
                    shared_rpc rpc
            );

            // Start accepting incoming connections
            void
            run() {
                if (!acceptor_.is_open())
                    return;
                do_accept();
            }

            void do_accept();

            void on_accept(boost::system::error_code ec);
        };
    }
}