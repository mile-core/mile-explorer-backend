#include "https/listener.hpp"

#include "utility.hpp"

namespace mile {
    namespace https {

        void listener::do_accept() {
            acceptor_.async_accept(
                    socket_,
                    std::bind(
                            &listener::on_accept,
                            shared_from_this(),
                            std::placeholders::_1));
        }

        void listener::on_accept(boost::system::error_code ec) {
            if (ec) {
                fail(ec, "accept");
            } else {
                // Create the session and run it
                std::make_shared<session>(std::move(socket_), ctx_, rpc)->run();
            }

            // Accept another connection
            do_accept();
        }

        listener::listener(boost::asio::io_context &ioc, ssl::context &ctx, tcp::endpoint endpoint, shared_rpc rpc) :
                ctx_(ctx),
                acceptor_(ioc),
                socket_(ioc),
                rpc(rpc) {
            boost::system::error_code ec;

            // Open the acceptor
            acceptor_.open(endpoint.protocol(), ec);
            if (ec) {
                fail(ec, "open");
                return;
            }

            // Allow address reuse
            acceptor_.set_option(boost::asio::socket_base::reuse_address(true), ec);
            if (ec) {
                fail(ec, "set_option");
                return;
            }

            // Bind to the server address
            acceptor_.bind(endpoint, ec);
            if (ec) {
                fail(ec, "bind");
                return;
            }

            // Start listening for connections
            acceptor_.listen(
                    boost::asio::socket_base::max_listen_connections, ec);
            if (ec) {
                fail(ec, "listen");
                return;
            }
        }

    }
}
