#include <vector>
#include <memory>
#include <exception>

#include <boost/asio/ssl/stream.hpp>
#include <boost/program_options.hpp>

#include <yaml-cpp/yaml.h>
#include <rethinkdb.h>

#include "https/listener.hpp"
#include "https/server_certificate.hpp"
#include "http/listener.hpp"

#include "router_api_v1.hpp"

namespace R = RethinkDB;

using tcp = boost::asio::ip::tcp;
namespace ssl =  boost::asio::ssl;


#ifdef __APPLE__

#else
#include <boost/stacktrace.hpp>

void terminate_handler() {
    std::cerr << "terminate called:"
              << std::endl
              << boost::stacktrace::stacktrace()
              << std::endl;
}

void signal_sigsegv(int signum){
    boost::stacktrace::stacktrace bt ;
    if(bt){
        std::cerr << "SIgnal"
                  << signum
                  << " , backtrace:"
                  << std::endl
                  << boost::stacktrace::stacktrace()
                  << std::endl;
    }
    std::abort();
}

#endif

int main(int argc, char **argv) {

#ifdef __APPLE__

#else
    ::signal(SIGSEGV,&signal_sigsegv);

    std::set_terminate(terminate_handler);
#endif

    boost::program_options::variables_map args_;
    boost::program_options::options_description app_options_;

    app_options_.add_options()
            ("help,h", "")
            ("config,c", boost::program_options::value<std::string>(), "config");
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, app_options_), args_);


    boost::program_options::notify(args_);

    if (args_.count("help")) {
        std::cout << app_options_ << std::endl;
        return 0;
    }


    boost::asio::ip::address address;
    unsigned short port_ssl;
    unsigned short port;
    int threads = 1;
    std::string rethink_db_ip;
    int rethink_db_port;
    std::string db_name;


    if (args_.count("config")) {

        auto config_path = args_["config"].as<std::string>();

        YAML::Node yaml_config = YAML::LoadFile(config_path);

        address = boost::asio::ip::make_address(yaml_config["address"].as<std::string>());
        port = yaml_config["port"].as<unsigned short>();
        port_ssl = yaml_config["port_ssl"].as<unsigned short>();
        yaml_config["threads"].as<int>();
        rethink_db_ip = yaml_config["rethink_db_host"].as<std::string>();
        rethink_db_port = yaml_config["rethink_db_port"].as<int>();
        db_name = yaml_config["db_name"].as<std::string>();

    } else {
        std::cerr<<"--config=config.yaml"<<std::endl;
        return 1;
    }


    auto conn = R::connect(rethink_db_ip, rethink_db_port);

    if (!conn) {
        std::cerr << "Could not connect to server\n";
        return 1;
    }

    std::cout << "Connected" << std::endl;

    auto rpc = std::make_shared<rpc_flat_map>();

   mile::router_api_v1(rpc,conn);


    boost::asio::io_context ioc{threads};

    ssl::context ctx{ssl::context::sslv23};

    mile::https::load_server_certificate(ctx);

    std::make_shared<mile::https::listener>(
            ioc,
            ctx,
            tcp::endpoint{address, port_ssl},
            rpc
    )->run();

    std::make_shared<mile::http::listener>(
            ioc,
            tcp::endpoint{address, port},
            rpc
    )->run();

    ioc.run();

    return EXIT_SUCCESS;
}