#include "router_api_v1.hpp"
#include <iostream>
#include <boost/algorithm/string.hpp>

#ifdef __APPLE__

#else
#include <boost/stacktrace.hpp>
#include <boost/exception/all.hpp>
#endif


namespace mile {

    namespace R = RethinkDB;
#ifdef __APPLE__

#else
    using  traced = boost::error_info<struct tag_stacktrace, boost::stacktrace::stacktrace>;

    template <class E>
    void throw_with_trace(const E& e) {
        throw boost::enable_error_info(e)<< traced(boost::stacktrace::stacktrace());
    }
#endif

    template<typename Error>
    inline auto trace(Error const & e){
#ifdef __APPLE__

#else
        std::cerr << "Excepts"<<std::endl;
        std::cerr << e.what() << std::endl;
        const boost::stacktrace::stacktrace* st = boost::get_error_info<traced>(e);
        if (st) {
            std::cerr << *st << std::endl;
        }
        std::cerr << "Except"<<std::endl;
#endif
    }


    void router_api_v1(std::shared_ptr<rpc_flat_map> rpc, std::unique_ptr<RethinkDB::Connection> &conn) {
        rpc->add(
                "get-block",
                [&](context &ctx) {
                    try {
                        auto block_id = ctx.request.params.at("id").get<uint64_t>();
                        auto cursor = R::db("blockchain").table("blocks").get(std::to_string(block_id)).run(*conn);
                        auto block = nlohmann::json::parse(cursor.to_datum().as_json());
                        block.erase("id");

                        if(block["signature"].is_string()) {
                            block.erase("signature");
                            block["signature"]=nlohmann::json::value_t::array;
                        }

                        ctx.response.result = block;

                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }
                }
        );


        rpc->add(
                "get-block-history",
                [&](context &ctx) {
                    try {

                        auto& params =  ctx.request.params;
                        auto begin_block_id = params.at("first-id").get<uint64_t>();
                        auto limit = params.at("limit").get<uint64_t>();
                        nlohmann::json filter;
                        if(params.find("filter-field") != params.end()){
                           filter = params.at("filter-field");
                        } else {
                            nlohmann::json tmp;
                            filter=tmp;
                        }


                        auto cursor = R::db("blockchain").table("blocks").between(begin_block_id, begin_block_id + limit, R::optargs("index", "block-id")).run(*conn);

                        nlohmann::json respons;

                        bool empty = filter.empty();

                        for (auto &i:cursor) {

                            auto tmp = nlohmann::json::parse(i.as_json());
                            if(!empty){
                                for(auto&j:filter){
                                    if( tmp.find(j) != tmp.end() ){
                                        tmp.erase(j.get<std::string>());
                                    }
                                }
                            }
                            respons.push_back(tmp);

                        }
                        ctx.response.result = respons;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }

                }
        );

        rpc->add(
                "get-transaction",
                [&](context &ctx) {
                    try {
                        auto public_key = ctx.request.params.at("public-key").get<std::string>();
                        auto id = ctx.request.params.at("id").get<uint64_t>();
                        std::cerr << "public-key = " << public_key << " id = " << std::to_string(id) << std::endl;
                        std::string query = public_key.append(":").append(std::to_string(id));
                        auto cursor = R::db("blockchain").table("transactions").get(query).run(*conn);
                        std::cerr << cursor.to_datum().as_json() << std::endl;
                        if(cursor.to_datum().is_nil()) {
                            ctx.response.result = nlohmann::json::array();
                        } else {
                            auto tmp = nlohmann::json::parse(cursor.to_datum().as_json());
                            tmp.erase("id");
                            ctx.response.result = tmp;
                        }
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result=nlohmann::json::array();
                    }
                }
        );

        rpc->add(
                "get-wallet-history-state",
                [&](context &ctx) {
                    try {
                        auto public_key = ctx.request.params.at("public-key").get<std::string>();
                        auto cursor_block = R::db("blockchain").table("wallets").get(public_key)["blocks"].count().run(*conn);
                        auto cursor_trx = R::db("blockchain").table("wallets").get(public_key)["transactions"].count().run(*conn);

                        nlohmann::json blocks;
                        blocks["count"] = static_cast<uint64_t >(*(cursor_block.to_datum().get_number()));
                        blocks["first-id"] = 0;

                        nlohmann::json trx;
                        trx["count"] = static_cast<uint64_t >(*(cursor_trx.to_datum().get_number()));
                        trx["first-id"] = 0;

                        ctx.response.result["block"] = blocks;
                        ctx.response.result["transaction"] = trx;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }

                }
        );

        rpc->add(
                "get-block-history-state",
                [&](context &ctx) {

                    try {
                        auto cursore = R::db("blockchain").table("blockchain_state").max(R::optargs("index", "block-id")).run(*conn);
                        auto state = nlohmann::json::parse(cursore.to_datum().as_json());
                        ctx.response.result["count"] = std::stoull(state["block-count"].get<std::string>());
                        ctx.response.result["first-id"] = 0;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }
                }
        );

        rpc->add(
                "get-wallet-history-blocks",
                [&](context &ctx) {
                    try {

                        auto public_key = ctx.request.params.at("public-key").get<std::string>();
                        auto first_id = ctx.request.params.at("first-id").get<uint64_t>();
                        auto limit = ctx.request.params.at("limit").get<uint64_t>();
                        auto cursore = R::db("blockchain").table("wallets").get(public_key)["blocks"].skip(first_id).limit(limit).run(*conn);

                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result=nlohmann::json::array();
                    }

                }
        );

        rpc->add(
                "get-wallet-history-transactions",
                [&](context &ctx) {

                    try {
                        auto public_key = ctx.request.params.at("public-key").get<std::string>();
                        auto first_id = ctx.request.params.at("first-id").get<uint64_t>();
                        auto limit = ctx.request.params.at("limit").get<uint64_t>();
                        auto cursore = R::db("blockchain").table("wallets").get(public_key)["transactions"].skip(first_id).limit(limit).run(*conn);
                        nlohmann::json trx;
                        for (auto &i:cursore) {
                            std::string my_input(*(i.get_string()));
                            std::vector<std::string> results;
                            boost::algorithm::split(results, my_input, boost::is_any_of(":"));
                            nlohmann::json element;
                            element["public-key"] = results[0];
                            element["id"] = std::stoull(results[1]);
                            trx.push_back(std::move(element));

                        }

                        ctx.response.result["transactions"] = trx;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }

                }
        );

        rpc->add(
                "help",
                [&](context &ctx) {
                    ctx.response.result = {
                            "get-block",
                            "get-block-history",
                            "get-transaction-info",
                            "get-wallet-history-state",
                            "get-block-history-state",
                            "get-wallet-history-blocks",
                            "get-wallet-history-transactions",
                            "get-nodes",
                            "get-network-state",
                            "get-transaction-history",
                            "get-transaction-history-state"

                    };
                }
        );


        rpc->add(
                "get-nodes",
                [&](context &ctx) {
                    try {
                        auto first_id = ctx.request.params.at("first-id").get<uint64_t>();
                        auto limit = ctx.request.params.at("limit").get<uint64_t>();
                        auto cursore = R::db("blockchain").table("nodes_state").max(R::optargs("index", "id"))["nodes"].skip(first_id).limit(limit).run(*conn);
                        ///auto cursore = R::db("blockchain").table("nodes_state").max(R::optargs("index", "id")).run(*conn);
                        auto state = nlohmann::json::parse(cursore.to_datum().as_json());
                        //auto id = state["id"].get<uint64_t>();
                        //state.erase("id");
                        ///state["block-id"] = id;
                        ctx.response.result = state;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }
                }
        );

        rpc->add(
                "get-network-state",
                [&](context &ctx) {
                    try {
                        auto cursore = R::db("blockchain").table("nodes_state").max(R::optargs("index", "id")).run(*conn);
                        auto state = nlohmann::json::parse(cursore.to_datum().as_json());
                        ctx.response.result["count"] = state["nodes"].size();
                        ctx.response.result["first-id"] = 0;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }
                }
        );


        rpc->add(
                "get-transaction-history",
                [&](context &ctx) {
                    try {
                        auto first_id = ctx.request.params.at("first-id").get<uint64_t>();
                        auto limit = ctx.request.params.at("limit").get<uint64_t>();

                        auto not_nil = [&](R::Var record) {
                            return (*record)["transactions"].ne(R::nil());
                        };

                        auto merge = [&](R::Var record) {
                            return (*record)["transactions"].merge(R::object("block-id", (*record)["id"]));
                        };

                        auto cursore = R::db("blockchain")
                                .table("transaction_stream")
                                .filter(not_nil)
                                .order_by(R::desc("id"))
                                .concat_map(merge)
                                .skip(first_id).limit(limit)
                                .run(*conn);

                        auto state = nlohmann::json::parse(cursore.to_datum().as_json());
                        ctx.response.result = state;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }
                }
        );

        rpc->add(
                "get-transaction-history-state",
                [&](context &ctx) {
                    try {
                        auto not_nil = [&](R::Var record) {
                            return (*record)["transactions"].ne(R::nil());
                        };

                        auto merge = [&](R::Var record) {
                            return (*record)["transactions"];
                        };

                        auto cursore = R::db("blockchain")
                                .table("transaction_stream")
                                .filter(not_nil)
                                .concat_map(merge)
                                .count()
                                .run(*conn);

                        ctx.response.result["nodes"]["count"] = uint64_t(*cursore.to_datum().get_number());
                        ctx.response.result["nodes"]["first-id"] = 0;
                    } catch(nlohmann::json::parse_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::invalid_iterator& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch(nlohmann::json::type_error & e){
                        trace(e);
                        make_response_parse_error(ctx);///
                    } catch(nlohmann::json::out_of_range& e){
                        trace(e);
                        make_response_parse_error(ctx);////
                    } catch(nlohmann::json::other_error& e){
                        trace(e);
                        make_response_parse_error(ctx);
                    } catch (...) {
                        ctx.response.result = nlohmann::json::array();
                    }
                }
        );

    }


}
