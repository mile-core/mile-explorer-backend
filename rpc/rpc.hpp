#pragma once

#include <string>
#include <unordered_map>
#include <functional>
#include <memory>

#include <boost/container/flat_map.hpp>

#include <jsonrpc/context.hpp>
#include <set>

using method = std::function<void(context&)>;

template<template<class...> class Container>
class rpc_base final {
public:
    using storage_type = Container<std::string,method>;

    rpc_base() = default;

    ~rpc_base() = default;

    bool apply(const std::string&name,context& ctx){
        auto it = storage.find(name);
        if( it == storage.end() ){
            return false;
        } else {
            it->second(ctx);
            return true;
        }
    }

    template <typename F>
    void add(std::string name, F&&f){
        storage.emplace(std::move(name), std::forward<F>(f));
    }

    std::set<std::string> keys() const {
        std::set<std::string> tmp;
        for(auto&i:storage){
            tmp.emplace(i.first);
        }
        return  tmp;
    }

private:
    storage_type storage;
};


using rpc_flat_map = rpc_base<boost::container::flat_map>;
using rpc_hash_map = rpc_base<std::unordered_map>;

using shared_rpc = std::shared_ptr<rpc_flat_map>;