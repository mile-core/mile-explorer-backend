cmake_minimum_required(VERSION 2.8)

project(mile-block-explorer CXX)

set(CMAKE_CXX_STANDARD 14)


message (STATUS "CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}")


if (ASAN)
    SET (CMAKE_CXX_COMPILER "/usr/bin/clang++")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-omit-frame-pointer")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address")

    message (STATUS "Building with AddressSanitizer")
endif ()

if (TSAN)
    SET (CMAKE_CXX_COMPILER "/usr/bin/clang++")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=thread")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=thread")

    message (STATUS "Building with ThreadSanitizer")
endif ()

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -std=c++14 -DBOOST_ASIO_HAS_MOVE -DBOOST_THREAD_USE_LIB -g -O0")#-DBOOST_ASIO_NO_EXTENSIONS")

IF(NOT APPLE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -pthread ")
ENDIF()

set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMakeModules")

include_directories(.)


include_directories(
        thirdparty/json/include/
)

#############################################################################################
#YAML
set(yaml_path thirdparty/yaml-cpp)
find_package(${YAML_INCLUDE_DIR} REQUIRED PATHS ${yaml_path} )
include_directories(SYSTEM thirdparty/yaml-cpp/include)
#YAML
#############################################################################################
#openssl

find_package(OpenSSL REQUIRED)
include_directories(${OPENSSL_INCLUDE_DIR})

#openssl
#############################################################################################
#rethinkdb

include_directories(
        thirdparty/librethinkdbxx/build/include
)

link_directories(${PROJECT_SOURCE_DIR}/thirdparty/librethinkdbxx/build)
#rethinkdb
#############################################################################################
#BOOST
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)

find_package (Boost  COMPONENTS REQUIRED
        system
        locale
        regex
        timer
        thread
        program_options
        filesystem
        serialization
)

include_directories (${Boost_INCLUDE_DIRS})

find_package(Threads)
#BOOST
#############################################################################################
#CCACHE
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)
#CCACHE
#############################################################################################

set(HEADER

        http/listener.hpp
        http/session.hpp

        router_api_v1.hpp
        utility.hpp

        https/listener.hpp
        https/server_certificate.hpp
        https/session.hpp

        jsonrpc/message.hpp

        rpc/rpc.hpp
)

set(SOURCER

        http/sourcer/listener.cpp
        http/sourcer/session.cpp

        https/source/listener.cpp
        https/source/session.cpp

        jsonrpc/context.cpp


        main.cpp
        router_api_v1.cpp
)

add_subdirectory(thirdparty)
add_subdirectory(example)

add_executable(${PROJECT_NAME} ${HEADER} ${SOURCER} )

target_link_libraries(
        ${PROJECT_NAME}
        ${CMAKE_THREAD_LIBS_INIT}
        ${Boost_LIBRARIES}
        ${OPENSSL_SSL_LIBRARY}
        ${OPENSSL_CRYPTO_LIBRARY}
        librethinkdb++.a
        dl
        yaml-cpp


)

