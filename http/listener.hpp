#pragma once

#include <boost/beast/http.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/steady_timer.hpp>


#include "utility.hpp"
#include "session.hpp"
#include "rpc/rpc.hpp"

namespace mile {
    namespace http {

        using tcp = boost::asio::ip::tcp;
        namespace http = boost::beast::http;


        class listener final : public std::enable_shared_from_this<listener> {
            tcp::acceptor acceptor_;
            tcp::socket socket_;
            shared_rpc rpc;

        public:
            listener(
                    boost::asio::io_context &ioc,
                    tcp::endpoint endpoint,
                    shared_rpc rpc
            );


            void run();

            void do_accept();

            void on_accept(boost::system::error_code ec);
        };
    }
}