#include "http/listener.hpp"
namespace mile {
namespace http {

    void listener::do_accept() {
        acceptor_.async_accept(
                socket_,
                std::bind(
                        &listener::on_accept,
                        shared_from_this(),
                        std::placeholders::_1));
    }


    void listener::on_accept(boost::system::error_code ec) {
        if(ec) {
            fail(ec, "accept");
        } else {
            // Create the session and run it
            std::make_shared<session>(std::move(socket_), rpc)->run();
        }

        // Accept another connection
        do_accept();
    }

    void listener::run() {
        if (!acceptor_.is_open())
            return;
        do_accept();
    }

    listener::listener(boost::asio::io_context &ioc, tcp::endpoint endpoint, shared_rpc rpc) :

            acceptor_(ioc),
            socket_(ioc),
            rpc(rpc) {
        boost::system::error_code ec;

        // Open the acceptor
        acceptor_.open(endpoint.protocol(), ec);
        if(ec) {
            fail(ec, "open");
            return;
        }

        // Bind to the server address
        acceptor_.bind(endpoint, ec);
        if(ec) {
            fail(ec, "bind");
            return;
        }

        // Start listening for connections
        acceptor_.listen(boost::asio::socket_base::max_listen_connections, ec);
        if(ec) {
            fail(ec, "listen");
            return;
        }
    }
    }
}
