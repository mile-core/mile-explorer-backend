#include "http/session.hpp"

namespace mile{ namespace http {

        void session::do_close() {
            // Send a TCP shutdown
            boost::system::error_code ec;
            socket_.shutdown(tcp::socket::shutdown_send, ec);

            // At this point the connection is closed gracefully
        }

        void session::on_write(boost::system::error_code ec, std::size_t bytes_transferred, bool close) {
            boost::ignore_unused(bytes_transferred);

            if (ec)
                return fail(ec, "write");

            if (close) {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return do_close();
            }

            // We're done with the response so delete it
            res_ = nullptr;

            // Read another request
            do_read();
        }

        void session::on_read(boost::system::error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);

            // This means they closed the connection
            if (ec == http::error::end_of_stream)
                return do_close();

            if (ec)
                return fail(ec, "read");

            // Send the response
            handle_request(std::move(req_), lambda_,rpc);
        }

        void session::do_read() {
            // Read a request
            http::async_read(socket_, buffer_, req_,
                             boost::asio::bind_executor(
                                     strand_,
                                     std::bind(
                                             &session::on_read,
                                             shared_from_this(),
                                             std::placeholders::_1,
                                             std::placeholders::_2)));
        }

        void session::run() {
            do_read();
        }

        session::send_lambda::send_lambda(session &self) : self_(self) {
        }
    }}