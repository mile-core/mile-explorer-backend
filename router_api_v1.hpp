#pragma once

#include <memory>
#include <rpc/rpc.hpp>
#include <rethinkdb.h>
namespace mile {
    void router_api_v1(std::shared_ptr<rpc_flat_map> rpc, std::unique_ptr<RethinkDB::Connection> &conn);
}