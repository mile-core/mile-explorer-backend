set(target get-block-history)

set(HEADER
       ${PROJECT_SOURCE_DIR}/example/client.hpp
)

set(SOURCER
        get-block-history.cpp
)
add_executable(${target} ${HEADER} ${SOURCER})


target_link_libraries(
        ${target}
        ${CMAKE_THREAD_LIBS_INIT}
        ${Boost_LIBRARIES}
        ${OPENSSL_SSL_LIBRARY}
        ${OPENSSL_CRYPTO_LIBRARY}
)
