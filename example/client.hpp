#pragma once

#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>


#include "utility.hpp"

namespace mile {
    namespace http {

        using tcp = boost::asio::ip::tcp;
        namespace ssl = boost::asio::ssl;
        namespace http = boost::beast::http;

        class https_client final : public std::enable_shared_from_this<https_client> {
            tcp::resolver resolver_;
            ssl::stream <tcp::socket> stream_;
            boost::beast::flat_buffer buffer_; // (Must persist between reads)
            http::request<http::string_body > req_;
            http::response<http::string_body> res_;
            std::function<void(http::response<http::string_body> response)> callback;

        public:
            // Resolver and stream require an io_context

            https_client(boost::asio::io_context &ioc, ssl::context &ctx)
                    : resolver_(ioc), stream_(ioc, ctx) {
            }

            ~https_client() = default;


            template<
                    typename REQUEST,
                    typename RESPONSE
            >
            void run(
                    char const *host,
                    char const *port,
                    REQUEST&& handler,
                    RESPONSE&& callback
                   ) {
                // Set SNI Hostname (many hosts need this to handshake successfully)
                if (!SSL_set_tlsext_host_name(stream_.native_handle(), host)) {
                    boost::system::error_code ec{static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category()};
                    std::cerr << ec.message() << "\n";
                    return;
                }

                this->callback=std::forward<RESPONSE>(callback);

                // Set up an HTTP GET request message
                req_.version(11);
                req_.method(http::verb::get);
                req_.set(http::field::host, host);

                handler(req_);

                std::cerr<< req_<<std::endl;

                // Look up the domain name
                resolver_.async_resolve(
                        host,
                        port,
                        std::bind(
                                &https_client::on_resolve,
                                shared_from_this(),
                                std::placeholders::_1,
                                std::placeholders::_2));
            }

            void
            on_resolve(
                    boost::system::error_code ec,
                    tcp::resolver::results_type results) {
                if (ec)
                    return fail(ec, "resolve");

                // Make the connection on the IP address we get from a lookup
                boost::asio::async_connect(
                        stream_.next_layer(),
                        results.begin(),
                        results.end(),
                        std::bind(
                                &https_client::on_connect,
                                shared_from_this(),
                                std::placeholders::_1));
            }

            void
            on_connect(boost::system::error_code ec) {
                if (ec)
                    return fail(ec, "connect");

                // Perform the SSL handshake
                stream_.async_handshake(
                        ssl::stream_base::client,
                        std::bind(
                                &https_client::on_handshake,
                                shared_from_this(),
                                std::placeholders::_1));
            }

            void
            on_handshake(boost::system::error_code ec) {
                if (ec)
                    return fail(ec, "handshake");

                // Send the HTTP request to the remote host
                http::async_write(stream_, req_,
                                  std::bind(
                                          &https_client::on_write,
                                          shared_from_this(),
                                          std::placeholders::_1,
                                          std::placeholders::_2));
            }

            void
            on_write(
                    boost::system::error_code ec,
                    std::size_t bytes_transferred) {
                boost::ignore_unused(bytes_transferred);

                if (ec)
                    return fail(ec, "write");

                // Receive the HTTP response
                http::async_read(stream_, buffer_, res_,
                                 std::bind(
                                         &https_client::on_read,
                                         shared_from_this(),
                                         std::placeholders::_1,
                                         std::placeholders::_2));
            }

            void
            on_read(
                    boost::system::error_code ec,
                    std::size_t bytes_transferred) {
                boost::ignore_unused(bytes_transferred);

                if (ec)
                    return fail(ec, "read");

               callback(std::move(res_));
                // Gracefully close the stream
                stream_.async_shutdown(
                        std::bind(
                                &https_client::on_shutdown,
                                shared_from_this(),
                                std::placeholders::_1));
            }

            void
            on_shutdown(boost::system::error_code ec) {
                if (ec == boost::asio::error::eof) {
                    // Rationale:
                    // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
                    ec.assign(0, ec.category());
                }
                if (ec)
                    return fail(ec, "shutdown");

                // If we get here then the connection is closed gracefully
            }
        };
    }
}