#include "example/client.hpp"

#include <boost/beast/http.hpp>

namespace http = boost::beast::http;
namespace ssl = boost::asio::ssl;

int main(int argc, char **argv) {

    boost::asio::io_context ioc;

    ssl::context ctx{ssl::context::sslv23_client};

    message::request_message request_;
    request_.id=12;
    request_.method="get-block";
    request_.params["id"]=0;

    std::make_shared<mile::http::https_client>(ioc,ctx)->run(
            "explorer.testnet.mile.global",
            "443",
            [&](http::request <http::string_body>& request){
                request.target("/v1/api");
                request.method(http::verb::post);
                request.body() = message::serialize(request_);
                request.prepare_payload();

            },
            [](http::response<http::string_body> response){
                std::cout << response << std::endl;
            }
    );

    ioc.run();

    return EXIT_SUCCESS;
}